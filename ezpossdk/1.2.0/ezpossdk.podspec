Pod::Spec.new do |spec|
    spec.name             = 'ezpossdk'
    spec.version          = '1.2.0'
    spec.summary          = 'EZ POS SDK for iOS devices'
    spec.license            = { :type => 'Copyright', :text => '©2017 The Payment House. All rights reserved.' }

    spec.description      = 'This document describes the interfaces between merchant applications and the eeZee Pay Server for the purpose of enabling Mmerchants from processing electronic payments through The Payment House’spec payment platform in their own applications and cash registers.'
                       

    spec.homepage         = 'http://developers.eezeepay.net/pos_sdk_ios/1.1/#introduction'
    spec.authors          = { 'Ivan Vishnev' => 'ivan@thepaymenthouse.com', 'Alexander Grischenko' => 'al.grischenko@gmail.com' }

    spec.ios.deployment_target = '9.0'
    spec.requires_arc     = true

    spec.source = {:http => 'https://test.eezeepay.net/ezpossdk_v1.2.zip'}
  
    spec.subspec 'pos' do |pos|

        pos.subspec 'sdk' do |sdk|
            sdk.vendored_frameworks    = 'eeZeePosSDK_iOS.framework'
            sdk.dependency               'ezpossdk/pos/internal/core'
            sdk.dependency               'ezpossdk/pos/internal/payment'
        end

        pos.subspec 'internal' do |internal|

            internal.subspec 'core' do |core|
                core.vendored_frameworks = 'TphCore_iOS.framework'
            end

            internal.subspec 'payment' do |payment|
                payment.vendored_frameworks = 'TphPaymentProcessor_iOS.framework'
                payment.frameworks = 'CoreLocation'
                payment.resources = ['mpos.core-resources.bundle', 'mpos-ui-resources.bundle']
                payment.dependency 'CocoaLumberjack', '~> 2.0.0'
                payment.dependency 'Lockbox', '~> 2.1.0'
            end

        end
    end
end