Pod::Spec.new do |s|
  s.name             = 'ezpossdk'
  s.version          = '1.2'
  s.summary          = 'EZ POS SDK for iOS devices'

  s.description      = 'This document describes the interfaces between merchant applications and the eeZee Pay Server for the purpose of enabling Mmerchants from processing electronic payments through The Payment House’s payment platform in their own applications and cash registers.'
                       

  s.homepage         = 'http://developers.eezeepay.net/pos_sdk_ios/1.1/#introduction'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.authors          = { "Ivan Vishnev" => "ivan@thepaymenthouse.com", "Alexander Grischenko" => "al.grischenko@gmail.com" }

  s.ios.deployment_target = '9.3'

  s.source = {:http => 'https://www.dropbox.com/s/o6uzhnj7txjeav5/ezpossdk_v1.2.zip?dl=1'}
  
  s.vendored_frameworks = 'eeZeePosSDK_iOS.framework'
  
end
